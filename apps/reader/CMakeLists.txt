################################################################################
##   PROJECT OPTIONS
################################################################################

project(${PROJECT_NAME}Reader)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)

################################################################################
##   EXECUTABLE
################################################################################

add_executable(${PROJECT_NAME})

add_dependencies(${PROJECT_NAME} ${CMAKE_PROJECT_NAME}Modules)

target_sources(${PROJECT_NAME}
	PUBLIC main.cpp
	)